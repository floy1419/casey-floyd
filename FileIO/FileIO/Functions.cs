﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace FileIO
{
    class Functions
    {
        public void writeToFile(Data data)
        {
            StreamWriter sw = new StreamWriter(data.file, true);
            if (data.win == true)
            {
                sw.WriteLine("Course Name: " + data.course + " | Player Name: " + data.name + " | Score: " + data.score + " | Date: " + data.day + " | Win: Yes");
            }
            else
            {
                sw.WriteLine("Course Name: " + data.course + " | Player Name: " + data.name + " | Score: " + data.score + " | Date: " + data.day + " | Win: No");

            }
            sw.Close();
        }

        public string[] readFromFile(Data data)
        {
            string[] results = File.ReadAllLines(data.file);
            return results;
        }

        public void clearFileContents(Data data)
        {
            File.WriteAllText(data.file, String.Empty);
        }
        public string getFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();
            string fileName = ofd.FileName;
            return fileName;
        }
    }
}
