﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FileIO
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnWrite_Click(object sender, RoutedEventArgs e)
        {
            Functions function = new Functions();
            Data data = new Data();
            data.file = txtFileName.Text;
            data.course = txtCourse.Text;
            data.name = txtPlayer.Text;
            data.score = Convert.ToInt32(txtScore.Text);
            data.day = DateTime.Parse(txtDate.Text);
            data.win = txtWin.Text == "y" ? true : false;
            function.writeToFile(data);
            txtPlayer.Clear();
            txtCourse.Clear();
            txtDate.Clear();
            txtScore.Clear();
            txtWin.Clear();
        }

        private void btnGetFile_Click(object sender, RoutedEventArgs e)
        {
            Functions function = new Functions();
            txtFileName.Text = function.getFile();
        }

        private void btnRead_Click(object sender, RoutedEventArgs e)
        {
            lstbxResults.Items.Clear();
            Functions function = new Functions();
            Data readData = new  Data();
            readData.file = txtFileName.Text;
            string[] lines = function.readFromFile(readData);
            foreach (string line in lines)
            {
                lstbxResults.Items.Add(line);
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            lstbxResults.Items.Clear();
            Functions function = new Functions();
            Data clearData = new Data();
            clearData.file = txtFileName.Text;
            function.clearFileContents(clearData);
        }
    }
}
