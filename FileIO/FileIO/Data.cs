﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class Data
    {
        public string file;
        public string course;
        public string name;
        public int score;
        public DateTime day;
        public bool win;
    }
}
