﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace GroupProjectCfAa
{
    class Books : Book
    {
        public string bookSearch(Book book)
        {
            
            string[] results = File.ReadAllLines("..//..//Books.txt");
            int length = results.Length;
            string[,] list = new String[length, 6];
            int i = 0;
            foreach (string readText in results)
            {
                string[] split = readText.Split(',');
                list[i, 0] = split[0];
                list[i, 1] = split[1];
                list[i, 2] = split[2];
                list[i, 3] = split[3];
                list[i, 4] = split[4];
                list[i, 5] = split[5];
                i++;
            }
            string books;
            int num = 0;
            bool titleSearch = false;
            int j = 0;
            while (j < (results.Length) && titleSearch == false)
            {
                books = list[j, 0];
                if (books == book.title)
                {
                    titleSearch = true;
                    num = j;
                }
                j++;
            }
            if (titleSearch == true)
            {
                string searchResult = (list[num, 0] + "," + list[num, 1] + "," + list[num, 2] + "," + list[num, 3] + "," + list[num, 4] + "," + list[num, 5]);
                return searchResult;
            }
            else
            {
                string noResult = (",,,,,");
                return noResult;
            }
        }
        public void removeBook(Book book)
        {
            string result = bookSearch(book);

            string[] list = File.ReadAllLines("..//..//Books.txt");

            int len = list.Length;

            File.WriteAllText("..//..//Books.txt", String.Empty);
            int j = 0;
            int spot = 0;
            while (j < len)
            {
                if (list[j] == result)
                {
                    spot = j;
                }
                j++;
            }
            while (spot < (len-1))
            {
                string temp = list[spot];
                list[spot] = list[spot + 1];
                spot++;
            }
            File.WriteAllLines("..//..//Books.txt",list);
            
        }
        public void addBook(Book book)
        {
            StreamWriter file = new StreamWriter("..//..//Books.txt", true);
            book.ISBN = book.checkISBN(book.ISBN);
            file.WriteLine((book.title + "," + book.author + "," + book.ISBN + "," + book.year + "," + book.genre + "," + book.publisher));
            file.Close();
        }
        public string[] bookReport()
        {
            string[] fileList = File.ReadAllLines("..//..//Books.txt");
            return fileList;
        }

    }
}
