﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GroupProjectCfAa
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Window
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            AddChoice page = new AddChoice();
            this.Close();
            page.Show();
        }

        private void editBook_Click(object sender, RoutedEventArgs e)
        {
            EditBook editPage = new EditBook();
            this.Close();
            editPage.Show();
        }

        private void removeBook_Click(object sender, RoutedEventArgs e)
        {
            RemoveBook removePage= new RemoveBook();
            this.Close();
            removePage.Show();
        }

        private void searchBooks_Click(object sender, RoutedEventArgs e)
        {
            SearchBooks searchPage = new SearchBooks();
            this.Close();
            searchPage.Show();
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            this.Close();
            main.Show();
        }
    }
}
