﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProjectCfAa
{
    class Books
    {
        /*
         * MS
         * A more object oriented way of doing this would be to construct 
         * the book that you found as an object and return the Book object,
         * not the string
         */
        public string bookSearch(string title)
        {
            string[,] books = new String[6, 6];
            int i = 0;
            /*
             * MS 
             * Text files should be included in the project (I have done this for Books.txt) 
             * and have the properties set to "Copy Always". 
             * This copies the file into the bin/debug folder where the .exe lives. It 
             * means you don't need a file path and you get a clean copy of the data every time. 
             * 
             * You should follow this pattern wherever you are reading / writing files. 
             */
            //foreach (string readText in File.ReadAllLines("..//..//Books.txt"))
            foreach (string readText in File.ReadAllLines("Books.txt"))
            {
                string[] split = readText.Split(',');
                books[i, 0] = split[0];
                books[i, 1] = split[1];
                books[i, 2] = split[2];
                books[i, 3] = split[3];
                books[i, 4] = split[4];
                books[i, 5] = split[5];
                i++;
            }
            string book;
            int num = 0;
            bool titleSearch = false;
            int j = 0;
            while (j < 6 && titleSearch == false)
            {
                book = books[j, 0];
                if (book == title)
                {
                    titleSearch = true;
                    num = j;
                }
                j++;
            }
            string result = (books[num, 0] + "," + books[num, 1] + "," + books[num, 2] + "," + books[num, 3] + "," + books[num, 4] + "," + books[num, 5]);
            return result;
        }
    }
}
