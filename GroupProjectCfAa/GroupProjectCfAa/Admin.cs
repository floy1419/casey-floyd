﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProjectCfAa
{
    class Admin
    {
        public string checkAdminLogin(Actor actor)
        {
            string[,] login = new String[3, 3];
            int i = 0;
            foreach (string readText in File.ReadAllLines("..//..//logins.txt"))
            {
                string[] split = readText.Split(',');
                login[i, 0] = split[0];
                login[i, 1] = split[1];
                login[i, 2] = split[2];
                i++;
            }

            bool unSearch = false;
            bool pwSearch = false;
            bool admin = false;
            string un;
            string pw = null;
            int number = 0;

            int j = 0;
            while (j < 3 && unSearch == false)
            {
                un = login[j, 0];
                if (un == actor.username)
                {
                    number = j;
                    unSearch = true;
                    pw = login[j, 1];
                }
                j++;
            }

            if (unSearch == true && pw == actor.password)
            {
                pwSearch = true;
            }
            if (login[number, 2] == "admin")
            {
                admin = true;
            }

            if (unSearch == true && pwSearch == true && admin == true)
            {
                return "approveLogin";
            }
            else
            {
                return "denyLogin";
            }
        }
        /*
         * MS 
         * It seems odd for the Admin object to be writing books to a data file. 
         * First, the object is "Admin" which is a person... not a book? Seems like handling books would be something
         * another object might do. An Admin object might hand it off to another object... but the actually doing 
         * of stuff to / with the book should be in another object. 
         * 
         * Second, the specific thing being done here is writing to a file. That's clearly data access work and should
         * not be mixed with the business logic in this class (the logic in the previous method is business logic.
         */
        public void addBook(string title, string author, string isbn, string year, string genre, string publisher)
        {
            StreamWriter file = new StreamWriter("..//..//Books.txt", true);
            file.WriteLine((title + "," + author + "," + isbn + "," + year + "," + genre + "," + publisher));
            file.Close();
        }

    }
    
}
