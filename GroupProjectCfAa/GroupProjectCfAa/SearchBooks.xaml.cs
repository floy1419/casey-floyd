﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GroupProjectCfAa
{
    /// <summary>
    /// Interaction logic for SearchBooks.xaml
    /// </summary>
    public partial class SearchBooks : Window
    {
        public SearchBooks()
        {
            InitializeComponent();
        }

        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            MainPage home = new MainPage();
            this.Close();
            home.Show();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            Books newBooks = new Books();
            string result = newBooks.bookSearch(txtSearch.Text);
            string[] split = result.Split(',');
            /*
             * MS
             * See my note in the Books object. You should be getting a book back
             * from that object and then you wouldn't be splitting up a string, you would be
             * reading properties from the object. That's more OO
             */
            txtResults.Text = ("Title - " + split[0] + "\nAuthor - " + split[1] + "\nISBN - " + split[2] + "\nYear - " + split[3] + "\nGenre - " + split[4] + "\nPublisher - " + split[5]);
        }
    }
}
