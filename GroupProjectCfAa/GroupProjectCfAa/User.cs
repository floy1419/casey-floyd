﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProjectCfAa
{
    class User
    {
        public string checkUserLogin(Actor actor)
        {
            string[,] login = new String[3, 3];
            int i = 0;
            foreach (string readText in File.ReadAllLines("..//..//logins.txt"))
            {
                string[] split = readText.Split(',');
                login[i, 0] = split[0];
                login[i, 1] = split[1];
                login[i, 2] = split[2];
                i++;
            }

            bool unSearch = false;
            bool pwSearch = false;
            bool user = false;
            string un;
            string pw = null;
            int number = 0;

            int j = 0;
            while (j < 3 && unSearch == false)
            {
                un = login[j, 0];
                if (un == actor.username)
                {
                    number = j;
                    unSearch = true;
                    pw = login[j, 1];
                }
                j++;
            }

            if (unSearch == true && pw == actor.password)
            {
                pwSearch = true;
            }
            if (login[number, 2] == "user")
            {
                user = true;
            }

            if (unSearch == true && pwSearch == true && user == true)
            {
                return "approveLogin";
            }
            else
            {
                return "denyLogin";
            }
        }
    }
}
