﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GroupProjectCfAa
{
    /// <summary>
    /// Interaction logic for AddBook.xaml
    /// </summary>
    public partial class AddBook : Window
    {
        public AddBook()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Books ab = new Books();
            Book book = new Book();
            book.author = txtAuthor.Text;
            book.genre = txtGenre.Text;
            book.title = txtTitle.Text;
            book.year = txtYear.Text;
            book.ISBN = txtISBN.Text;
            book.publisher = txtPublisher.Text;
            ab.addBook(book);
            MessageBox.Show("Book Added Succesfully");
            txtAuthor.Clear();
            txtGenre.Clear();
            txtISBN.Clear();
            txtPublisher.Clear();
            txtTitle.Clear();
            txtYear.Clear();
            txtTitle.Focus();
        }

        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            MainPage home = new MainPage();
            this.Close();
            home.Show();
        }
    }
}
