﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroupProjectCfAa
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Actor actor = new Actor();
            actor.username = txtUsername.Text;
            actor.password = txtPassword.Password.ToString();
            
            if (radAdmin.IsChecked == true)
            {
                Admin newAdmin = new Admin();
                string login = newAdmin.checkAdminLogin(actor);
                if (login == "approveLogin")
                {
                    MainPage home = new MainPage();
                    this.Hide();
                    home.Closed += (s, args) => this.Close();
                    home.Show();
                }
                else
                {
                    MessageBox.Show("Login Unsuccesful. Try Again");
                    txtUsername.Clear();
                    txtPassword.Clear();
                }
            }
            else if (radUser.IsChecked == true)
            {
                User newUser = new User();
                string login = newUser.checkUserLogin(actor);
                if (login == "approveLogin")
                {
                    UserPage home = new UserPage();
                    this.Hide();
                    home.Closed += (s, args) => this.Close();
                    home.Show();
                }
                else
                {
                    MessageBox.Show("Login Unsuccesful. Try Again");
                    txtUsername.Clear();
                    txtPassword.Clear();
                    txtUsername.Focus();
                }
            }
            else
            {
                MessageBox.Show("Must Select Admin or User");

            }
            


        }

    }

}
